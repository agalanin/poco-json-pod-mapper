# poco-json-pod-mapper

## About

**poco-json-pod-mapper** is a non-intrusive header-only C++11 library for map
JSON structures produced by [POCO library](https://pocoproject.org) JSON parser
to Plain Old Data structures and back.

Features:

* Mapping JSON-to-POD and POD-to-JSON by one command.
* Syntax and type checking.
* Unlimited depth of structure mapping.
* Built-in support for mapping basic data types (integer, float, string,
  boolean) and standard containers.
* Define converters for custom data types easily (as objects or basic types).
* Integrate with existing parsers and serializers at any position in JSON tree.
* Support for dynamic JSON subtrees via Poco::Dynamic.

License: MIT/X11

Project home: https://bitbucket.org/agalanin/poco-json-pod-mapper

Repository mirror: http://galanin.nnov.ru/hg/poco-json-pod-mapper

If you want more examples, take a look at the test suite in *test* directory.

## Mapping JSON to POD

### Basic usage

```C++
#include <json_to_pod/mapper.hpp>
#include <Poco/Dynamic/Var.h>

...

Poco::JSON::Parser parser;
auto parsedJson = parser.parse(jsonString);
YourPodType pod;
json_to_pod::map(parsedJson, pod);
```

It is enough to convert
```JSON
[ [ 1, 2, 3 ], [ 4, 5, 6 ] ]
```
into
```C++
std::list<std::vector<int>>
```

See next chapters for information how to define mappings for any data type.

### Defining structure mapping

To define mapping for your structure make template specialization for
```C++
namespace json_to_pod
{
template < typename T >
struct StructMapper
{
    static void mapStruct(const SourceAdapter & from, T & to);
};
}
```
Structure may have two distinct field (member) types:

* Required - mapping will fail if JSON object member is not found.
* Optional - if JSON object member is not found then structure field is left unchanged.

Method reference:

```c++
template < typename T, typename MapperClass = Mapper<T> >
void json_to_pod::mapRequiredField(const SourceAdapter & from, const std::string & key, T & to);
```

Map required field.

* **T** is a mapped type.
* **MapperClass** is a class with method *map* that performs real mapping. By default *json_to_pod::Mapper<T>* is used.
* **from** is a wrapper for a source Poco::JSON::Object or Poco::DynamicStruct depending on a input structure content.
* **key** is an object field name.
* **to** is a destination variable.

```c++
template < typename T, typename MapperClass = Mapper<T> >
bool json_to_pod::mapOptionalField(const SourceAdapter & from, const std::string & key, T & to);
```
Map optional field.

* **T** is a mapped type.
* **MapperClass** is a class with method *map* that performs real mapping. By default *json_to_pod::Mapper<T>* is used.
* **from** is a wrapper for a source Poco::JSON::Object or Poco::DynamicStruct depending on a input structure content.
* **key** is an object field name.
* **to** is a destination variable.
* return value is *true* if field is present and *false* otherwise.

Example:
```C++
struct MyStruct
{
    int integer;
    std::vector<std::string> names;
    std::optional<int> optional; // C++17
};

namespace json_to_pod
{
template <>
struct StructMapper<MyStruct>
{
static void mapStruct(const SourceAdapter & from, MyStruct & to)
{
    mapRequiredField(from, "int", to.integer);
    mapRequiredField(from, "names", to.names);
    mapOptionalField(from, "optional", to.optional);
}
};
}
```

And now we able to successfully map the following JSON data to MyStruct instance
(*optional* member is absent):
```JSON
{
    "int": 22,
    "names": [ "Athos", "Porthos", "Aramis" ]
}
```


### Defining mapping for basic types

Sometimes we need to parse data represented as a string. For example, JSON may
contain date/time field in format "2019-11-17 17:44:15Z" that we want to map
into Poco::DateTime variable.

Let specialize the following template:
```C++
namespace json_to_pod
{
template < typename T >
struct Mapper
{
    static void map(const Poco::Dynamic::Var & from, T & to);
};
}
```

So mapping between date/time as a string and Poco::DateTime may be defined as follows:
```C++
namespace json_to_pod
{
template <>
struct Mapper<Poco::DateTime>
{
static void map(const Poco::Dynamic::Var & from, Poco::DateTime & to)
{
    if (from.type() != typeid(std::string))
        throw bad_type_exception("string");
    std::string str;
    from.convert(str);
    int tzd;
    to = Poco::DateTimeParser::parse("%Y-%m-%d %H:%M:%S %Z", str, tzd);
    to.makeUTC(tzd);
}
};
}
```

### Inheritance

Simply call mapper for base class(es):

```c++
namespace json_to_pod
{
template <>
struct StructMapper<DerivedStruct>
{
    static void mapStruct(const SourceAdapter & from, DerivedStruct & to)
    {
        StructMapper<BaseStruct1>::mapStruct(from, to);
        StructMapper<BaseStruct2>::mapStruct(from, to);
        ...
    }
};

static void map(const Poco::Dynamic::Var & from, DerivedType & to)
{
    Mapper<BaseType1>::map(from, to);
    Mapper<BaseType2>::map(from, to);
    ...
}
}
```

### Validation

Throw an exception if validation failed. Upper-level structure or vector mapper
will re-throw the exception with additional information about erroneous element
location in the source JSON tree.

```c++
struct Eigenvector
{
    int x, y;
};

namespace json_to_pod
{
template <>
struct StructMapper<Eigenvector>
{
static void mapStruct(const SourceAdapter & from, Eigenvector & to)
{
    mapRequiredField(from, "x", to.x);
    mapRequiredField(from, "y", to.y);

    if (x == 0 && y == 0)
        throw parse_error("eigenvector must be non-zero");
    }
};
}
```

### Exceptions

There are some pre-defined exception types:

* *field_not_found* - required object member is absent;
* *bad_type_exception* - unsupported field type (for example, mapping of a
  string into int16_t);
* *parse_error* - error while parsing object member/array item.

### Pre-defined mappings

#### <json_to_pod/mapper.hpp>

Basic types: bool, int16_t, int32_t, int64_t, uint16_t, uint32_t, uint64_t,
std::string. Note that mappings for char, int8_t and uint8_t are not defined by
default due to ambiguosity.

#### <json_to_pod/std/memory.hpp>

Mappings for std::unique_ptr and std::shared_ptr. If JSON object is 'null' then
smart pointer is unset.

#### <json_to_pod/std/optional.hpp>

Mapping for std::optional (C++17 is required). If JSON object is 'null' then
value is unset.

#### <json_to_pod/std/list.hpp>
#### <json_to_pod/std/set.hpp>
#### <json_to_pod/std/unordered_set.hpp>
#### <json_to_pod/std/vector.hpp>

Standard containers (std:list, std::set, std::unordered_set, std::vector).

#### <json_to_pod/std/map.hpp>

Default mapping for JSON object as source and std::map<std::string, T> as
destination. Every JSON object field is a key and field content is a value
of a map entry.
```json
{
    "key1" : {
        "a": 12,
        "b": "foo",
    },
    "key2" : {
        "a": 34,
        "b": "bar",
    }
}
```

#### <json_to_pod/map_common.hpp>

```c++
template < const char * KeyLabel, const char * ValueLabel, typename T >
struct PairArrayToMapMapper;
```

Custom mapper for mapping JSON array to std::map/std::unordered_map with
any key and any mapped types. *KeyLabel* and *ValueLabel* template
parameters are object field names:

```json
[
    { "key" : 1, "value" : "one" },
    { "key" : 2, "value" : "two" },
    { "key" : 3, "value" : "three" }
]
```

Example (a bit terrifying):

```c++
static const char Key [] = "key";
static const char Value [] = "value";
...
// inside json_to_pod::StructMapper<Type>::mapStruct implementation
mapRequiredField<decltype(to.submap), PairArrayToMapMapper<Key, Value, decltype(to.submap)>>(from, "submap", to.submap);
...
// ... or in top-level mapping function call
std::map<int, std::string> value;
json_to_pod::PairArrayToMapMapper<Key, Value, decltype(value)>::map(tree, value);
```

#### <json_to_pod/boost/optional.hpp>

Mapping for boost::optional. If JSON object is 'null' then value is unset.

#### <json_to_pod/boost/shared_ptr.hpp>

Mapping for boost::shared_ptr. If JSON object is 'null' then smart pointer is
unset.

#### <json_to_pod/Poco/Optional.hpp>

Mapping for Poco::Optional. If JSON object is 'null' then value is unset.

#### <json_to_pod/Poco/JSON/Object.hpp>
#### <json_to_pod/Poco/Dynamic/Var.hpp>
#### <json_to_pod/Poco/Dynamic/DynamicStruct.hpp>

Useful mappings if you want to keep JSON sub-tree as-is.

## Mapping POD to JSON

### Basic usage

```C++
#include <pod_to_json/mapper.hpp>
#include <Poco/Dynamic/Var.h>
#include <Poco/JSON/Stringifier.h>

...

YourPodType pod;
Poco::Dynamic::Var tree;
pod_to_json::map(pod, tree);
Poco::JSON::Stringifier::stringify(tree, std::cout);
```

As for JSON-to-POD mapping you need to specialize templates for your custom
types.

### Defining structure mapping

Function reference:

```c++
template < typename T, typename MapperClass = Mapper<T> >
void pod_to_json::mapField(TargetAdapter & to, const std::string & key, const T & from);
```
Map structure field.

* **T** is a mapped type.
* **MapperClass** is a class with method *map* that performs real mapping. By default *json_to_pod::Mapper<T>* is used.
* **to** is a wrapper for a destination Poco::JSON::Object or Poco::DynamicStruct depending on a call chain.
* **key** is an object field name.
* **value** is a source variable.

Template that need to be specialized:
```C++
namespace pod_to_json
{
template < typename T >
struct StructMapper
{
    static void mapStruct(const T & from, TargetAdapter & to);
};
}
```

Example:
```C++
struct SimpleStruct
{
    int intField;
    std::string strField;
};
namespace pod_to_json
{
template <>
struct StructMapper<SimpleStruct>
{
    static void mapStruct(const SimpleStruct & from, TargetAdapter & to)
    {
        mapField(to, "int", from.intField);
        mapField(to, "str", from.strField);
    }
};
}
```
Result:
```JSON
{
    "int": 22,
    "str": "hello"
}
```

### Defining mapping for basic types

Template for specialization:
```C++
namespace pod_to_json
{
template < typename T >
struct Mapper
{
    static void map(const T & from, Poco::Dynamic::Var & to);
}
}
```

Example (mapping Poco::DateTime into string representation):
```C++
namespace pod_to_json
{
template <>
struct Mapper<Poco::DateTime>
{
    static void map(const Poco::DateTime & from, Poco::Dynamic::Var & to)
    {
        to = Poco::DateTimeFormatter::format(from, "%Y-%m-%d %H:%M:%SZ");
    }
};
}
```

### Pre-defined mappings

#### <pod_to_json/mapper.hpp>

Basic types: bool, int16_t, int32_t, int64_t, uint16_t, uint32_t, uint64_t,
std::string. Note that mappings for char, int8_t and uint8_t are not defined by
default due to ambiguosity.

#### <pod_to_json/std/optional.hpp>
#### <pod_to_json/std/memory.hpp>
#### <pod_to_json/boost/optional.hpp>
#### <pod_to_json/boost/shared_ptr.hpp>
#### <pod_to_json/Poco/Optional.hpp>

Optional values and smart pointers. Empty optional value and NULL pointers are
mapped into 'null'.

#### <pod_to_json/std/list.hpp>
#### <pod_to_json/std/set.hpp>
#### <pod_to_json/std/unordered_set.hpp>
#### <pod_to_json/std/vector.hpp>

Standard containers (std:list, std::set, std::unordered_set, std::vector).

#### <pod_to_json/std/map.hpp>

Default mapping for std::map<std::string, T> as a source and JSON object a
destination. Every JSON object field is a key and field content is a value of
a map entry.
```json
{
    "key1" : {
        "a": 12,
        "b": "foo",
    },
    "key2" : {
        "a": 34,
        "b": "bar",
    }
}
```

#### <pod_to_json/map_common.hpp>

```c++
template < const char * KeyLabel, const char * ValueLabel, typename T >
struct MapToPairArrayMapper;
```

Custom mapper for mapping std::map/std::unordered_map with any key and
any mapped types into JSON array of objects.
*KeyLabel* and *ValueLabel* template parameters are object field names:

```json
[
    { "key" : 1, "value" : "one" },
    { "key" : 2, "value" : "two" },
    { "key" : 3, "value" : "three" }
]
```

Example:

```c++
static const char Key [] = "key";
static const char Value [] = "value";
...
// inside pod_to_json::StructMapper<Type>::mapStruct
mapField<decltype(from.submap), MapToPairArrayMapper<Key, Value, decltype(from.submap)>>(to, "submap", from.submap);
// ... or in top-level mapping function call
std::map<int, std::string> value;
Poco::Dynamic::Var tree;
pod_to_json::MapToPairArrayMapper<Key, Value, decltype(value)>::map(value, tree);
```

#### <pod_to_json/Poco/JSON/Object.hpp>
#### <pod_to_json/Poco/Dynamic/Var.hpp>
#### <pod_to_json/Poco/Dynamic/Struct.hpp>

A way to map subtree back to JSON.
