//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019,2023 Alexander Galanin <al@galanin.nnov.ru>
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include <exception>
#include <functional>
#include <stdexcept>
#include <string>
#include <vector>

#include <Poco/Dynamic/Var.h>
#include <Poco/JSON/JSON.h>
#include <Poco/JSON/Parser.h>
#include <Poco/Version.h>

#include "version.hpp"

static_assert(POCO_VERSION >= 0x01090000, "POCO >= 1.9.0 is required");

namespace json_to_pod
{

class field_not_found : public std::invalid_argument
{
public:

    field_not_found(const std::string & name) :
        std::invalid_argument("field not found: " + name)
    {}
};

class bad_type_exception : public std::invalid_argument
{
public:

    const std::string expected_type;

    bad_type_exception(const char *expected_type_) :
        std::invalid_argument(std::string("is not a ") + expected_type_),
        expected_type(expected_type_)
    {}
};

class parse_error : public std::invalid_argument
{
public:

    explicit parse_error(const std::string & msg) :
        std::invalid_argument(msg)
    {}
};

class SourceAdapter
{
    const std::function<bool (const std::string &)> _has;
    const std::function<Poco::Dynamic::Var (const std::string &)> _get;
    const std::function<std::vector<std::string> ()> _names;
public:
    SourceAdapter(const Poco::JSON::Object & obj) :
        _has([&obj](const std::string & key) { return obj.has(key); }),
        _get([&obj](const std::string & key) { return obj.get(key); }),
        _names([&obj]() { return obj.getNames(); })
    {}

    SourceAdapter(const Poco::DynamicStruct & ds) :
        _has([&ds](const std::string & key) { return ds.contains(key); }),
        _get([&ds](const std::string & key) { return ds[key]; }),
        _names([&ds]() {
            const auto members = ds.members();
            return std::vector<std::string>(members.begin(), members.end());
        })
    {}

    bool has(const std::string & key) const { return _has(key); }
    Poco::Dynamic::Var get(const std::string & key) const { return _get(key); }
    std::vector<std::string> names() const { return _names(); }
};

template < typename T >
struct StructMapper
{
    static void mapStruct(const SourceAdapter & obj, T & to) = delete;
};

inline SourceAdapter castToObject(const Poco::Dynamic::Var & from)
{
    if (from.type() == typeid(Poco::JSON::Object::Ptr))
    {
        Poco::JSON::Object::Ptr obj = from.extract<Poco::JSON::Object::Ptr>();
        return *obj;
    }
    else if (from.type() == typeid(Poco::DynamicStruct))
    {
        const Poco::DynamicStruct & obj = from.extract<Poco::DynamicStruct>();
        return obj;
    }
    else
        throw bad_type_exception("object");
}

template < typename T >
struct Mapper
{
    static void map(const Poco::Dynamic::Var & from, T & to)
    {
        auto obj = castToObject(from);
        StructMapper<T>::mapStruct(obj, to);
    }
};

template < typename T >
void map(const Poco::Dynamic::Var & from, T & to)
{
    Mapper<T>::map(from, to);
}

template < typename T >
void map(const Poco::DynamicStruct & from, T & to)
{
    StructMapper<T>::mapStruct(from, to);
}

template < typename T >
void mapSignedInteger(const Poco::Dynamic::Var & from, T & to)
{
    if (from.type() == typeid(Poco::Int16) ||
        from.type() == typeid(Poco::Int32) ||
        from.type() == typeid(Poco::Int64) ||
        from.type() == typeid(Poco::UInt16) ||
        from.type() == typeid(Poco::UInt32) ||
        from.type() == typeid(Poco::UInt64))
    {
        from.convert(to);
    }
    else
        throw bad_type_exception("signed integer");
}

template < typename T >
void mapUnsignedInteger(const Poco::Dynamic::Var & from, T & to)
{
    bool th = false;
    if (from.type() == typeid(Poco::UInt16) ||
        from.type() == typeid(Poco::UInt32) ||
        from.type() == typeid(Poco::UInt64))
    {
        from.convert(to);
    }
    else if (from.type() == typeid(Poco::Int16))
        th = from.extract<Poco::Int16>() < 0;
    else if (from.type() == typeid(Poco::Int32))
        th = from.extract<Poco::Int32>() < 0;
    else if (from.type() == typeid(Poco::Int64))
        th = from.extract<Poco::Int64>() < 0;
    else
        th = true;
    if (th)
        throw bad_type_exception("unsigned integer");
    else
        from.convert(to);
}

template <>
struct Mapper<int16_t>
{
    static void map(const Poco::Dynamic::Var & from, int16_t & to)
    {
        mapSignedInteger(from, to);
    }
};

template <>
struct Mapper<int32_t>
{
    static void map(const Poco::Dynamic::Var & from, int32_t & to)
    {
        mapSignedInteger(from, to);
    }
};

template <>
struct Mapper<int64_t>
{
    static void map(const Poco::Dynamic::Var & from, int64_t & to)
    {
        mapSignedInteger(from, to);
    }
};

template <>
struct Mapper<uint16_t>
{
    static void map(const Poco::Dynamic::Var & from, uint16_t & to)
    {
        mapUnsignedInteger(from, to);
    }
};

template <>
struct Mapper<uint32_t>
{
    static void map(const Poco::Dynamic::Var & from, uint32_t & to)
    {
        mapUnsignedInteger(from, to);
    }
};

template <>
struct Mapper<uint64_t>
{
    static void map(const Poco::Dynamic::Var & from, uint64_t & to)
    {
        mapUnsignedInteger(from, to);
    }
};

template < typename T >
void mapFloat(const Poco::Dynamic::Var & from, T & to)
{
    if (from.type() == typeid(Poco::Int16) ||
        from.type() == typeid(Poco::Int32) ||
        from.type() == typeid(Poco::Int64) ||
        from.type() == typeid(Poco::UInt16) ||
        from.type() == typeid(Poco::UInt32) ||
        from.type() == typeid(Poco::UInt64) ||
        from.type() == typeid(float) ||
        from.type() == typeid(double))
    {
        from.convert(to);
    }
    else
        throw bad_type_exception("float");
}

template <>
struct Mapper<float>
{
    static void map(const Poco::Dynamic::Var & from, float & to)
    {
        mapFloat(from, to);
    }
};

template <>
struct Mapper<double>
{
    static void map(const Poco::Dynamic::Var & from, double & to)
    {
        mapFloat(from, to);
    }
};

template <>
struct Mapper<bool>
{
    static void map(const Poco::Dynamic::Var & from, bool & to)
    {
        if (from.type() != typeid(bool))
            throw bad_type_exception("boolean");
        from.convert(to);
    }
};

template <>
struct Mapper<std::string>
{
    static void map(const Poco::Dynamic::Var & from, std::string & to)
    {
        if (from.type() != typeid(std::string))
            throw bad_type_exception("string");
        from.convert(to);
    }
};

template < typename T, typename MapperClass = Mapper<T> >
void mapStructField(const SourceAdapter & obj, const std::string & key, T & to)
try
{
    MapperClass::map(obj.get(key), to);
}
catch (const Poco::Exception &e)
{
    std::throw_with_nested(parse_error("field " + key + ": " + e.displayText()));
}
catch (const std::exception &e)
{
    std::throw_with_nested(parse_error("field " + key + ": " + e.what()));
}

template < typename T, typename MapperClass = Mapper<T> >
void mapRequiredField(const SourceAdapter & from, const std::string & key, T & to)
{
    if (!from.has(key))
        throw field_not_found(key);
    mapStructField<T, MapperClass>(from, key, to);
}

template < typename T, typename MapperClass = Mapper<T> >
bool mapOptionalField(const SourceAdapter & from, const std::string & key, T & to)
{
    if (from.has(key))
    {
        mapStructField<T, MapperClass>(from, key, to);
        return true;
    }
    else
        return false;
}

}
