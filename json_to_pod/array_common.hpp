//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Alexander Galanin <al@galanin.nnov.ru>
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "mapper.hpp"

#include <vector>

namespace json_to_pod
{

class ArrayAdapter
{
    const std::function<size_t ()> _size;
    const std::function<Poco::Dynamic::Var (size_t)> _get;
public:
    ArrayAdapter(const Poco::JSON::Array & arr) :
        _size([&arr]() { return arr.size(); }),
        _get([&arr](size_t i) { return arr.get(static_cast<unsigned int>(i)); })
    {}

    ArrayAdapter(const std::vector<Poco::Dynamic::Var> & arr) :
        _size([&arr]() { return arr.size(); }),
        _get([&arr](size_t i) { return arr.at(i); })
    {}

    size_t size() const { return _size(); }
    Poco::Dynamic::Var get(size_t i) const { return _get(i); }
};

inline ArrayAdapter castToArray(const Poco::Dynamic::Var &from)
{
    if (from.type() == typeid(Poco::JSON::Array::Ptr))
    {
        Poco::JSON::Array::Ptr arrp = from.extract<Poco::JSON::Array::Ptr>();
        Poco::JSON::Array & arr = *arrp;

        return arr;
    }
    else if (from.type() == typeid(std::vector<Poco::Dynamic::Var>))
    {
        const std::vector<Poco::Dynamic::Var> & arr = from.extract<std::vector<Poco::Dynamic::Var>>();

        return arr;
    }
    else
        throw bad_type_exception("array");
}

template < typename E >
void mapArrayElement(const ArrayAdapter & arr, size_t i, E & item)
try
{
    Mapper<E>::map(arr.get(i), item);
}
catch (const Poco::Exception &e)
{
    std::throw_with_nested(parse_error("item " + std::to_string(i) + ": " + e.displayText()));
}
catch (const std::exception &e)
{
    std::throw_with_nested(parse_error("item " + std::to_string(i) + ": " + e.what()));
}

template < class T >
void mapArrayToContainer(const Poco::Dynamic::Var & from, T & to)
{
    auto arr = castToArray(from);
    to.clear();
    for (size_t i = 0; i < arr.size(); ++i)
    {
        typename T::value_type item;
        mapArrayElement(arr, i, item);
        to.insert(to.end(), std::move(item));
    }
}

};
