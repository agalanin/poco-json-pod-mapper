//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Alexander Galanin <al@galanin.nnov.ru>
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "mapper.hpp"
#include "array_common.hpp"

#include <utility>

namespace json_to_pod
{

template < class MapT >
static void mapStructureToMap(const SourceAdapter & obj, MapT & to)
{
    to.clear();

    const auto keys = obj.names();
    for(const auto & key : keys)
    {
        typename MapT::mapped_type value;
        mapStructField(obj, key, value);
        to.insert(std::make_pair(key, value));
    }
}

template < class T1, class T2, const char * Label1, const char * Label2 >
struct MapKeyValueItem : public std::pair<T1, T2> {};

template < class T1, class T2, const char * Label1, const char * Label2 >
struct StructMapper<MapKeyValueItem<T1, T2, Label1, Label2>>
{
    static void mapStruct(const SourceAdapter & from, MapKeyValueItem<T1, T2, Label1, Label2> & to)
    {
        mapRequiredField(from, Label1, to.first);
        mapRequiredField(from, Label2, to.second);
    }
};

template < const char * KeyLabel, const char * ValueLabel, class T >
void mapArrayToMap(const Poco::Dynamic::Var & from, T & to)
{
    using KeyValuePair = MapKeyValueItem<typename T::key_type, typename T::mapped_type, KeyLabel, ValueLabel>;

    auto arr = castToArray(from);
    to.clear();
    for (size_t i = 0; i < arr.size(); ++i)
    {
        KeyValuePair p;
        mapArrayElement(arr, i, p);
        to.insert(p);
    }
}

template < const char * KeyLabel, const char * ValueLabel, typename T >
struct PairArrayToMapMapper
{
    static void map(const Poco::Dynamic::Var & from, T & to)
    {
        mapArrayToMap<KeyLabel, ValueLabel>(from, to);
    }
};

};
