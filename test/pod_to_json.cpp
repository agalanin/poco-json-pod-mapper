#include <boost/test/unit_test.hpp>

#include <iostream>
#include <sstream>

#include <pod_to_json/mapper.hpp>
#include <pod_to_json/map_common.hpp>

#include <pod_to_json/std/list.hpp>
#include <pod_to_json/std/map.hpp>
#include <pod_to_json/std/memory.hpp>
#include <pod_to_json/std/set.hpp>
#include <pod_to_json/std/unordered_map.hpp>
#include <pod_to_json/std/unordered_set.hpp>
#include <pod_to_json/std/vector.hpp>

#if __cplusplus >= 201703L
#   include <pod_to_json/std/optional.hpp>
#endif

#include <pod_to_json/boost/optional.hpp>
#include <pod_to_json/boost/shared_ptr.hpp>

#include <pod_to_json/Poco/Dynamic/Struct.hpp>
#include <pod_to_json/Poco/Dynamic/Var.hpp>
#include <pod_to_json/Poco/JSON/Object.hpp>
#include <pod_to_json/Poco/Optional.hpp>

#include <Poco/DateTimeFormatter.h>
#include <Poco/JSON/Stringifier.h>

namespace
{

struct SimpleStruct
{
    int intField;
    std::string strField;
};

struct OptionalStruct
{
#if __cplusplus >= 201703L
    std::optional<int> std_optional;
#endif
    std::unique_ptr<int> std_unique_ptr;
    std::shared_ptr<int> std_shared_ptr;

    Poco::Optional<int> poco_optional;

    boost::optional<int> boost_optional;
    boost::shared_ptr<int> boost_shared_ptr;
};

struct DynamicStruct
{
    Poco::Dynamic::Var v_number;
    Poco::Dynamic::Var v_float;
    Poco::Dynamic::Var v_string;
    Poco::Dynamic::Var v_boolean;
    Poco::Dynamic::Var v_array;
    Poco::Dynamic::Var v_object;
    Poco::Dynamic::Var v_null;
    Poco::Dynamic::Var v_opt_null;
};

struct StructWithMap2
{
    std::map<int, std::string> submap;
};

}

static const char Key [] = "key";
static const char Value [] = "value";

namespace pod_to_json
{

template <>
struct StructMapper<SimpleStruct>
{
    static void mapStruct(const SimpleStruct & from, TargetAdapter & to)
    {
        mapField(to, "int", from.intField);
        mapField(to, "str", from.strField);
    }
};

template <>
struct StructMapper<OptionalStruct>
{
    static void mapStruct(const OptionalStruct & from, TargetAdapter & to)
    {
#if __cplusplus >= 201703L
        mapField(to, "std_optional", from.std_optional);
#endif
        mapField(to, "std_unique_ptr", from.std_unique_ptr);
        mapField(to, "std_shared_ptr", from.std_shared_ptr);
        mapField(to, "poco_optional", from.poco_optional);
        mapField(to, "boost_optional", from.boost_optional);
        mapField(to, "boost_shared_ptr", from.boost_shared_ptr);
    }
};

template <>
struct StructMapper<DynamicStruct>
{
    static void mapStruct(const DynamicStruct & from, TargetAdapter & to)
    {
        mapField(to, "number", from.v_number);
        mapField(to, "float", from.v_float);
        mapField(to, "string", from.v_string);
        mapField(to, "boolean", from.v_boolean);
        mapField(to, "array", from.v_array);
        mapField(to, "object", from.v_object);
        mapField(to, "null", from.v_null);
        mapField(to, "opt_null", from.v_opt_null);
    }
};

template <>
struct StructMapper<StructWithMap2>
{
    static void mapStruct(const StructWithMap2 & from, TargetAdapter & to)
    {
        mapField<decltype(from.submap), MapToPairArrayMapper<Key, Value, decltype(from.submap)>>(to, "submap", from.submap);
    }
};

template <>
struct Mapper<Poco::DateTime>
{
    static void map(const Poco::DateTime & from, Poco::Dynamic::Var & to)
    {
        to = Poco::DateTimeFormatter::format(from, "%Y-%m-%d %H:%M:%SZ");
    }
};

}

class PodToJsonFixture
{
public:
    std::string convert(const Poco::Dynamic::Var & value)
    {
        std::stringstream ss;
        Poco::JSON::Stringifier::stringify(value, ss);
        return ss.str();
    }

};

BOOST_FIXTURE_TEST_SUITE(MapPodToJson, PodToJsonFixture)

using namespace pod_to_json;

BOOST_AUTO_TEST_CASE(IntMapping)
{
    int16_t int16 = -300;
    int32_t int32 = -33000;
    int64_t int64 = -5000000000;
    uint16_t uint16 = 300;
    uint32_t uint32 = 33000;
    uint64_t uint64 = 18446744073709551615ULL;

    Poco::Dynamic::Var tree;
    tree.clear(); map(int16, tree); BOOST_CHECK_EQUAL(convert(tree), "-300");
    tree.clear(); map(int32, tree); BOOST_CHECK_EQUAL(convert(tree), "-33000");
    tree.clear(); map(int64, tree); BOOST_CHECK_EQUAL(convert(tree), "-5000000000");
    tree.clear(); map(uint16, tree); BOOST_CHECK_EQUAL(convert(tree), "300");
    tree.clear(); map(uint32, tree); BOOST_CHECK_EQUAL(convert(tree), "33000");
    tree.clear(); map(uint64, tree); BOOST_CHECK_EQUAL(convert(tree), "18446744073709551615");
}

BOOST_AUTO_TEST_CASE(FloatMapping)
{
    float  floatValue  = 3.1415927f;
    double doubleValue = 2.718281828;

    Poco::Dynamic::Var tree;
    tree.clear(); map(floatValue, tree);  BOOST_CHECK_EQUAL(convert(tree), "3.1415927");
    tree.clear(); map(doubleValue, tree); BOOST_CHECK_EQUAL(convert(tree), "2.718281828");
}

BOOST_AUTO_TEST_CASE(BooleanMapping)
{
    bool trueValue = true;
    bool falseValue = false;

    Poco::Dynamic::Var tree;
    tree.clear(); map(trueValue, tree);  BOOST_CHECK_EQUAL(convert(tree), "true");
    tree.clear(); map(falseValue, tree); BOOST_CHECK_EQUAL(convert(tree), "false");
}

BOOST_AUTO_TEST_CASE(StringMapping)
{
    std::string value = "foobar";

    Poco::Dynamic::Var tree;
    map(value, tree);
    BOOST_CHECK_EQUAL(convert(tree), "\"foobar\"");
}

BOOST_AUTO_TEST_CASE(VectorInt)
{
    std::vector<int> value{ 2, 7, 1828, 1828 };

    Poco::Dynamic::Var tree;
    map(value, tree);
    BOOST_CHECK_EQUAL(convert(tree), "[ 2, 7, 1828, 1828 ]");
}

BOOST_AUTO_TEST_CASE(VectorVectorInt)
{
    std::vector<std::vector<int>> value{
        { 3, 14, 15, 92, 7 },
        { 2, 7, 1828, 1828 }
    };

    Poco::Dynamic::Var tree;
    map(value, tree);
    BOOST_CHECK_EQUAL(convert(tree), "[ [ 3, 14, 15, 92, 7 ], [ 2, 7, 1828, 1828 ] ]");
}

BOOST_AUTO_TEST_CASE(ListInt)
{
    std::list<int> value{ 2, 7, 1828, 1828 };

    Poco::Dynamic::Var tree;
    map(value, tree);
    BOOST_CHECK_EQUAL(convert(tree), "[ 2, 7, 1828, 1828 ]");
}

BOOST_AUTO_TEST_CASE(VectorListInt)
{
    std::vector<std::list<int>> value{
        { 3, 14, 15, 92, 7 },
        { 2, 7, 1828, 1828 }
    };

    Poco::Dynamic::Var tree;
    map(value, tree);
    BOOST_CHECK_EQUAL(convert(tree), "[ [ 3, 14, 15, 92, 7 ], [ 2, 7, 1828, 1828 ] ]");
}

BOOST_AUTO_TEST_CASE(SetInt)
{
    std::set<int> value{ 2, 7, 1828, 1828 };

    Poco::Dynamic::Var tree;
    map(value, tree);
    BOOST_CHECK_EQUAL(convert(tree), "[ 2, 7, 1828 ]");
}

BOOST_AUTO_TEST_CASE(UnorderedSetInt)
{
    std::unordered_set<int> value{ 2, 7, 1828, 1828 };

    Poco::Dynamic::Var tree;
    map(value, tree);
    BOOST_CHECK_EQUAL(convert(tree), "[ 1828, 7, 2 ]");
}

BOOST_AUTO_TEST_CASE(MapStringIntToObject)
{
    std::map<std::string, int> value{ { "a", 1 }, { "b", 2 } };

    Poco::Dynamic::Var tree;
    map(value, tree);
    BOOST_CHECK_EQUAL(convert(tree), R"({"a":1,"b":2})");
}

BOOST_AUTO_TEST_CASE(MapStringStructToObject)
{
    std::map<std::string, SimpleStruct> value{
        { "a", { 1, "aa" } },
        { "b", { 2, "bb" } }
    };

    Poco::Dynamic::Var tree;
    map(value, tree);
    BOOST_CHECK_EQUAL(convert(tree), R"({"a":{"int":1,"str":"aa"},"b":{"int":2,"str":"bb"}})");
}

BOOST_AUTO_TEST_CASE(MapStringIntToDynamicStruct)
{
    std::map<std::string, int> value{ { "a", 1 }, { "b", 2 } };

    Poco::DynamicStruct dest;
    map(value, dest);
    BOOST_REQUIRE_EQUAL(dest.size(), 2);
    BOOST_CHECK_EQUAL(dest["a"].extract<int>(), 1);
    BOOST_CHECK_EQUAL(dest["b"].extract<int>(), 2);
}

BOOST_AUTO_TEST_CASE(UnorderedMapStringIntObject)
{
    std::unordered_map<std::string, int> value{ { "a", 1 }, { "b", 2 } };

    Poco::Dynamic::Var tree;
    map(value, tree);
    BOOST_CHECK_EQUAL(convert(tree), R"({"b":2,"a":1})");
}

BOOST_AUTO_TEST_CASE(MapToJsonArrayOfObjects)
{
    std::map<int, std::string> value {
        { 1, "one" },
        { 2, "two" },
        { 3, "three" }
    };

    Poco::Dynamic::Var tree;
    MapToPairArrayMapper<Key, Value, decltype(value)>::map(value, tree);
    BOOST_CHECK_EQUAL(convert(tree), R"([ {
  "key" : 1,
  "value" : "one"
}, {
  "key" : 2,
  "value" : "two"
}, {
  "key" : 3,
  "value" : "three"
} ])");
}

BOOST_AUTO_TEST_CASE(SubMapToJsonArrayOfObjects)
{
    StructWithMap2 value;
    value.submap = {
        { 1, "one" },
        { 2, "two" },
        { 3, "three" }
    };

    Poco::Dynamic::Var tree;
    map(value, tree);
    BOOST_CHECK_EQUAL(convert(tree), R"({"submap":[ {
  "key" : 1,
  "value" : "one"
}, {
  "key" : 2,
  "value" : "two"
}, {
  "key" : 3,
  "value" : "three"
} ]})");
}

BOOST_AUTO_TEST_CASE(SimpleStructMapping)
{
    SimpleStruct value;
    value.intField = 22;
    value.strField = "blah";

    Poco::Dynamic::Var tree;
    map(value, tree);
    BOOST_CHECK_EQUAL(convert(tree), R"({"int":22,"str":"blah"})");
}

BOOST_AUTO_TEST_CASE(OptionalStructNotNull)
{
    OptionalStruct value;
#if __cplusplus >= 201703L
    value.std_optional = 1;
#endif
    value.std_unique_ptr.reset(new int(2));
    value.std_shared_ptr.reset(new int(3));
    value.poco_optional = 4;
    value.boost_optional = 5;
    value.boost_shared_ptr.reset(new int (6));

    Poco::Dynamic::Var tree;
    map(value, tree);
#if __cplusplus >= 201703L
    BOOST_CHECK_EQUAL(convert(tree), R"({"std_optional":1,"std_unique_ptr":2,"std_shared_ptr":3,"poco_optional":4,"boost_optional":5,"boost_shared_ptr":6})");
#else
    BOOST_CHECK_EQUAL(convert(tree), R"({"std_unique_ptr":2,"std_shared_ptr":3,"poco_optional":4,"boost_optional":5,"boost_shared_ptr":6})");
#endif
}

BOOST_AUTO_TEST_CASE(OptionalStructNull)
{
    OptionalStruct value;

    Poco::Dynamic::Var tree;
    map(value, tree);
#if __cplusplus >= 201703L
    BOOST_CHECK_EQUAL(convert(tree), R"({"std_optional":null,"std_unique_ptr":null,"std_shared_ptr":null,"poco_optional":null,"boost_optional":null,"boost_shared_ptr":null})");
#else
    BOOST_CHECK_EQUAL(convert(tree), R"({"std_unique_ptr":null,"std_shared_ptr":null,"poco_optional":null,"boost_optional":null,"boost_shared_ptr":null})");
#endif
}

BOOST_AUTO_TEST_CASE(CustomType)
{
    // Sun Nov 17 14:10:46 GMT 2019
    Poco::DateTime value(Poco::Timestamp::fromEpochTime(1573999846));

    Poco::Dynamic::Var tree;
    map(value, tree);
    BOOST_CHECK_EQUAL(convert(tree), "\"2019-11-17 14:10:46Z\"");
}

BOOST_AUTO_TEST_CASE(PocoDynamicVar)
{
    DynamicStruct value;
    value.v_number = 123;
    value.v_float = 45.67;
    value.v_string = "blah";
    value.v_boolean = true;
    value.v_null.clear();

    Poco::Dynamic::Array arr{ 2, 12, 22 };
    value.v_array = std::move(arr);

    Poco::JSON::Object obj;
    obj.set("a", 1);
    obj.set("b", 2);
    value.v_object = std::move(obj);

    Poco::Dynamic::Var tree;
    map(value, tree);
    BOOST_CHECK_EQUAL(convert(tree), R"({"number":123,"float":45.67,"string":"blah","boolean":true,"array":[ 2, 12, 22 ],"object":{"a":1,"b":2},"null":null,"opt_null":null})");
}

BOOST_AUTO_TEST_CASE(PocoDynamicStructDestination)
{
    SimpleStruct value;
    value.intField = 22;
    value.strField = "hello";

    Poco::DynamicStruct dest;
    map(value, dest);

    BOOST_REQUIRE(dest.contains("int"));
    BOOST_CHECK_EQUAL(dest["int"].extract<int>(), 22);
    BOOST_REQUIRE(dest.contains("str"));
    BOOST_CHECK_EQUAL(dest["str"].extract<std::string>(), "hello");
}

BOOST_AUTO_TEST_CASE(PocoJsonObjectDestination)
{
    SimpleStruct value;
    value.intField = 22;
    value.strField = "hello";

    Poco::JSON::Object dest;
    map(value, dest);

    BOOST_REQUIRE(dest.has("int"));
    BOOST_CHECK_EQUAL(dest.get("int").extract<int>(), 22);
    BOOST_REQUIRE(dest.has("str"));
    BOOST_CHECK_EQUAL(dest.get("str").extract<std::string>(), "hello");
}

BOOST_AUTO_TEST_CASE(PocoDynamicStructWithArray)
{
    Poco::DynamicStruct value;
    Poco::Dynamic::Array arr{ 2, 12, 22 };
    value["array"] = arr;

    Poco::Dynamic::Var tree;
    map(value, tree);
    BOOST_CHECK_EQUAL(convert(tree), R"({ "array" : [ 2, 12, 22 ] })");
}

BOOST_AUTO_TEST_CASE(PocoDynamicStructWithArrayToDynamicStruct)
{
    Poco::DynamicStruct value;
    value["int"] = 22;
    value["str"] = "hello";

    Poco::DynamicStruct dest;
    map(value, dest);

    BOOST_REQUIRE(dest.contains("int"));
    BOOST_CHECK_EQUAL(dest["int"].extract<int>(), 22);
    BOOST_REQUIRE(dest.contains("str"));
    BOOST_CHECK_EQUAL(dest["str"].extract<std::string>(), "hello");
}

BOOST_AUTO_TEST_CASE(PocoJsonObjectToJson)
{
    Poco::JSON::Object obj;
    obj.set("int", 22);
    obj.set("str", "hello");

    Poco::Dynamic::Var dest;
    map(obj, dest);

    BOOST_CHECK_EQUAL(convert(dest), R"({"int":22,"str":"hello"})");
}

BOOST_AUTO_TEST_CASE(PocoJsonObjectToDynamicStruct)
{
    Poco::JSON::Object obj;
    obj.set("int", 22);
    obj.set("str", "hello");

    Poco::DynamicStruct dest;
    map(obj, dest);

    BOOST_REQUIRE(dest.contains("int"));
    BOOST_CHECK_EQUAL(dest["int"].extract<int>(), 22);
    BOOST_REQUIRE(dest.contains("str"));
    BOOST_CHECK_EQUAL(dest["str"].extract<std::string>(), "hello");
}

BOOST_AUTO_TEST_SUITE_END()
