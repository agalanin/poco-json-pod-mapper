#include <boost/test/unit_test.hpp>

#include <json_to_pod/mapper.hpp>

#include <json_to_pod/std/list.hpp>
#include <json_to_pod/std/map.hpp>
#include <json_to_pod/std/memory.hpp>
#include <json_to_pod/std/set.hpp>
#include <json_to_pod/std/unordered_map.hpp>
#include <json_to_pod/std/unordered_set.hpp>
#include <json_to_pod/std/vector.hpp>

#include <json_to_pod/boost/optional.hpp>
#include <json_to_pod/boost/shared_ptr.hpp>

#include <json_to_pod/Poco/Dynamic/Struct.hpp>
#include <json_to_pod/Poco/Dynamic/Var.hpp>
#include <json_to_pod/Poco/JSON/Object.hpp>
#include <json_to_pod/Poco/Optional.hpp>

#if __cplusplus >= 201703L
#   include <json_to_pod/std/optional.hpp>
#endif

#include <Poco/DateTimeParser.h>

namespace
{

struct SimpleStruct
{
    int intField;
    std::string strField;

    bool operator == (const SimpleStruct & b) {
        return intField == b.intField && strField == b.strField;
    }
    bool operator != (const SimpleStruct & b) { return !(*this == b); }

    friend std::ostream & operator << (std::ostream & out, const SimpleStruct & b) {
        return out << "int=" << b.intField << " str=" << b.strField;
    }
};

struct NumberStruct
{
    std::vector<int> numbers;
};

struct OptionalStruct
{
#if __cplusplus >= 201703L
    std::optional<int> std_optional;
#endif
    std::unique_ptr<int> std_unique_ptr;
    std::shared_ptr<int> std_shared_ptr;

    Poco::Optional<int> poco_optional;

    boost::optional<int> boost_optional;
    boost::shared_ptr<int> boost_shared_ptr;
};

struct DynamicStruct
{
    Poco::Dynamic::Var v_number;
    Poco::Dynamic::Var v_float;
    Poco::Dynamic::Var v_string;
    Poco::Dynamic::Var v_boolean;
    Poco::Dynamic::Var v_array;
    Poco::Dynamic::Var v_object;
    Poco::Dynamic::Var v_null;
    Poco::Dynamic::Var v_opt_null;
};

struct ListAndSimple
{
    std::list<int> num;
    SimpleStruct sub;
};

struct StructWithMap
{
    std::map<std::string, int> submap;
};

struct StructWithMap2
{
    std::map<int, std::string> submap;
};

struct AlternatingStruct
{
    std::string realName{ "unknown" };
    std::string pseudoName{ "unknown" };
};

struct StructWithJsonObject
{
    Poco::JSON::Object obj;
};

}

static const char Key [] = "key";
static const char Value [] = "value";

namespace json_to_pod
{

template <>
struct StructMapper<SimpleStruct>
{
    static void mapStruct(const SourceAdapter & from, SimpleStruct & to)
    {
        mapRequiredField(from, "int", to.intField);
        mapOptionalField(from, "str", to.strField);
    }
};

template <>
struct StructMapper<NumberStruct>
{
    static void mapStruct(const SourceAdapter & from, NumberStruct & to)
    {
        mapRequiredField(from, "numbers", to.numbers);
    }
};

template <>
struct StructMapper<OptionalStruct>
{
    static void mapStruct(const SourceAdapter & from, OptionalStruct & to)
    {
#if __cplusplus >= 201703L
        mapRequiredField(from, "std_optional", to.std_optional);
#endif
        mapRequiredField(from, "std_unique_ptr", to.std_unique_ptr);
        mapRequiredField(from, "std_shared_ptr", to.std_shared_ptr);
        mapRequiredField(from, "poco_optional", to.poco_optional);
        mapRequiredField(from, "boost_optional", to.boost_optional);
        mapRequiredField(from, "boost_shared_ptr", to.boost_shared_ptr);
    }
};

template <>
struct StructMapper<DynamicStruct>
{
    static void mapStruct(const SourceAdapter & from, DynamicStruct & to)
    {
        mapRequiredField(from, "number", to.v_number);
        mapRequiredField(from, "float", to.v_float);
        mapRequiredField(from, "string", to.v_string);
        mapRequiredField(from, "boolean", to.v_boolean);
        mapRequiredField(from, "array", to.v_array);
        mapRequiredField(from, "object", to.v_object);
        mapRequiredField(from, "null", to.v_null);
        mapOptionalField(from, "opt_null", to.v_opt_null);
    }
};

template <>
struct StructMapper<ListAndSimple>
{
    static void mapStruct(const SourceAdapter & from, ListAndSimple & to)
    {
        mapRequiredField(from, "num", to.num);
        mapRequiredField(from, "sub", to.sub);
    }
};

template <>
struct StructMapper<StructWithMap>
{
    static void mapStruct(const SourceAdapter & from, StructWithMap & to)
    {
        mapRequiredField(from, "submap", to.submap);
    }
};

template <>
struct StructMapper<StructWithMap2>
{
    static void mapStruct(const SourceAdapter & from, StructWithMap2 & to)
    {
        mapRequiredField<decltype(to.submap), PairArrayToMapMapper<Key, Value, decltype(to.submap)>>(from, "submap", to.submap);
    }
};

template <>
struct StructMapper<AlternatingStruct>
{
    static void mapStruct(const SourceAdapter & from, AlternatingStruct & to)
    {
        bool realDefined = mapOptionalField(from, "real_name", to.realName);
        bool pseudoDefined = mapOptionalField(from, "pseudo_name", to.pseudoName);
        if (!realDefined && !pseudoDefined)
            throw parse_error("real or pseudo name must be defined");
    }
};

template <>
struct StructMapper<StructWithJsonObject>
{
    static void mapStruct(const SourceAdapter & from, StructWithJsonObject & to)
    {
        mapRequiredField(from, "obj", to.obj);
    }
};

template <>
struct Mapper<Poco::DateTime>
{
    static void map(const Poco::Dynamic::Var & from, Poco::DateTime & to)
    {
        if (from.type() != typeid(std::string))
            throw bad_type_exception("string");
        std::string str;
        from.convert(str);
        int tzd;
        to = Poco::DateTimeParser::parse("%Y-%m-%d %H:%M:%S %Z", str, tzd);
        to.makeUTC(tzd);
    }
};

}

////////////////////////////////////////////////////////////////////////////

class JsonToPodFixture
{
    Poco::JSON::Parser parser;
public:
    static bool intCheck(const json_to_pod::bad_type_exception & e) {
        return e.expected_type == "signed integer";
    }
    static bool uintCheck(const json_to_pod::bad_type_exception & e) { 
        return e.expected_type == "unsigned integer";
    }
    static bool floatCheck(const json_to_pod::bad_type_exception & e) { 
        return e.expected_type == "float";
    }
    static bool boolCheck(const json_to_pod::bad_type_exception & e) { 
        return e.expected_type == "boolean";
    }
    static bool stringCheck(const json_to_pod::bad_type_exception & e) { 
        return e.expected_type == "string";
    }
    static bool structCheck(const json_to_pod::bad_type_exception & e) { 
        return e.expected_type == "object";
    }
    static bool arrCheck(const json_to_pod::bad_type_exception & e) { 
        return e.expected_type == "array";
    }
    static bool trueCheck(const std::exception &) { 
        return true;
    }
    static bool tooLargeCheck(const Poco::Exception &e) { 
        return e.displayText() == "Out of range: Value too large.";
    }
    static bool tooSmallCheck(const Poco::Exception &e) { 
        return e.displayText() == "Out of range: Value too small.";
    }

    Poco::Dynamic::Var parse(const std::string &json)
    {
        return parser.parse(json);
    }
};

BOOST_FIXTURE_TEST_SUITE(MapJsonToPod, JsonToPodFixture)

using namespace json_to_pod;

BOOST_AUTO_TEST_CASE(IntTypeCast)
{
    static auto json = R"({
        "number"    : 123,
        "float"     : 45.67,
        "string"    : "blah",
        "boolean"   : true,
        "array"     : [ 2, 12, 22 ],
        "object"    : { "a" : 1, "b" : 2 },
        "null"      : null
    })";
    auto tree = parse(json);
    auto obj = tree.extract<Poco::JSON::Object::Ptr>();

    int value = 0;

    BOOST_CHECK_EXCEPTION(map(obj->get("float"), value), bad_type_exception, intCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("string"), value), bad_type_exception, intCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("boolean"), value), bad_type_exception, intCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("array"), value), bad_type_exception, intCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("object"), value), bad_type_exception, intCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("null"), value), bad_type_exception, intCheck);

    map(obj->get("number"), value);
    BOOST_CHECK_EQUAL(value, 123);
}

BOOST_AUTO_TEST_CASE(IntUIntDynamicTypeCast)
{
    bool b = false;
    int16_t int16 = 0;
    int32_t int32 = 0;
    int64_t int64 = 0;
    uint16_t uint16 = 0;
    uint32_t uint32 = 0;
    uint64_t uint64 = 0;
    float fl = 0;
    double db = 0;

    int64_t i;
    uint64_t ui;

    BOOST_CHECK_EXCEPTION(map(Poco::Dynamic::Var(b), i), bad_type_exception, trueCheck);
    map(Poco::Dynamic::Var(int16), i);
    map(Poco::Dynamic::Var(int32), i);
    map(Poco::Dynamic::Var(int64), i);
    map(Poco::Dynamic::Var(uint16), i);
    map(Poco::Dynamic::Var(uint32), i);
    map(Poco::Dynamic::Var(uint64), i);
    BOOST_CHECK_EXCEPTION(map(Poco::Dynamic::Var(fl), i), bad_type_exception, trueCheck);
    BOOST_CHECK_EXCEPTION(map(Poco::Dynamic::Var(db), i), bad_type_exception, trueCheck);

    BOOST_CHECK_EXCEPTION(map(Poco::Dynamic::Var(b), ui), bad_type_exception, trueCheck);
    map(Poco::Dynamic::Var(int16), ui);
    map(Poco::Dynamic::Var(int32), ui);
    map(Poco::Dynamic::Var(int64), ui);
    map(Poco::Dynamic::Var(uint16), ui);
    map(Poco::Dynamic::Var(uint32), ui);
    map(Poco::Dynamic::Var(uint64), ui);
    BOOST_CHECK_EXCEPTION(map(Poco::Dynamic::Var(fl), ui), bad_type_exception, trueCheck);
    BOOST_CHECK_EXCEPTION(map(Poco::Dynamic::Var(db), ui), bad_type_exception, trueCheck);
}

BOOST_AUTO_TEST_CASE(FloatTypeCast)
{
    static auto json = R"({
        "number"    : 123,
        "uint64"    : 18446744073709551615,
        "float"     : 45.67,
        "string"    : "blah",
        "boolean"   : true,
        "array"     : [ 2, 12, 22 ],
        "object"    : { "a" : 1, "b" : 2 },
        "null"      : null
    })";
    auto tree = parse(json);
    auto obj = tree.extract<Poco::JSON::Object::Ptr>();

    float value = 0;

    BOOST_CHECK_EXCEPTION(map(obj->get("string"), value), bad_type_exception, floatCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("boolean"), value), bad_type_exception, floatCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("array"), value), bad_type_exception, floatCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("object"), value), bad_type_exception, floatCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("null"), value), bad_type_exception, floatCheck);

    map(obj->get("number"), value);
    BOOST_CHECK_EQUAL(value, 123);

    map(obj->get("uint64"), value);
    BOOST_CHECK_EQUAL(value, 18446744073709551615ULL);

    map(obj->get("float"), value);
    BOOST_TEST(value == 45.67, boost::test_tools::tolerance(1e-6));
}

BOOST_AUTO_TEST_CASE(DoubleTypeCast)
{
    static auto json = R"({
        "number"    : 123,
        "uint64"    : 18446744073709551615,
        "float"     : 45.67,
        "string"    : "blah",
        "boolean"   : true,
        "array"     : [ 2, 12, 22 ],
        "object"    : { "a" : 1, "b" : 2 },
        "null"      : null
    })";
    auto tree = parse(json);
    auto obj = tree.extract<Poco::JSON::Object::Ptr>();

    double value = 0;

    BOOST_CHECK_EXCEPTION(map(obj->get("string"), value), bad_type_exception, floatCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("boolean"), value), bad_type_exception, floatCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("array"), value), bad_type_exception, floatCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("object"), value), bad_type_exception, floatCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("null"), value), bad_type_exception, floatCheck);

    map(obj->get("number"), value);
    BOOST_CHECK_EQUAL(value, 123);

    map(obj->get("uint64"), value);
    BOOST_CHECK_EQUAL(value, 18446744073709551615ULL);

    map(obj->get("float"), value);
    BOOST_TEST(value == 45.67, boost::test_tools::tolerance(1e-6));
}

BOOST_AUTO_TEST_CASE(FloatDoubleDynamicTypeCast)
{
    bool b = false;
    int16_t int16 = 0;
    int32_t int32 = 0;
    int64_t int64 = 0;
    uint16_t uint16 = 0;
    uint32_t uint32 = 0;
    uint64_t uint64 = 0;
    float fl = 0;
    double db = 0;

    float f;
    double d;

    BOOST_CHECK_EXCEPTION(map(Poco::Dynamic::Var(b), f), bad_type_exception, trueCheck);
    map(Poco::Dynamic::Var(int16), f);
    map(Poco::Dynamic::Var(int32), f);
    map(Poco::Dynamic::Var(int64), f);
    map(Poco::Dynamic::Var(uint16), f);
    map(Poco::Dynamic::Var(uint32), f);
    map(Poco::Dynamic::Var(uint64), f);
    map(Poco::Dynamic::Var(fl), f);
    map(Poco::Dynamic::Var(db), f);

    BOOST_CHECK_EXCEPTION(map(Poco::Dynamic::Var(b), d), bad_type_exception, trueCheck);
    map(Poco::Dynamic::Var(int16), d);
    map(Poco::Dynamic::Var(int32), d);
    map(Poco::Dynamic::Var(int64), d);
    map(Poco::Dynamic::Var(uint16), d);
    map(Poco::Dynamic::Var(uint32), d);
    map(Poco::Dynamic::Var(uint64), d);
    map(Poco::Dynamic::Var(fl), d);
    map(Poco::Dynamic::Var(db), d);
}

BOOST_AUTO_TEST_CASE(Int16RangeCheck)
{
    static auto json = R"({
        "int8p"  :  3,
        "int8n"  : -3,
        "int16p" :  200,
        "int16n" : -200,
        "int32p" :  33000,
        "int32n" : -33000,
        "int64p" :  3000000000,
        "int64n" : -3000000000,
        "uint64" : 18446744073709551615
    })";
    auto tree = parse(json);
    auto obj = tree.extract<Poco::JSON::Object::Ptr>();
    int16_t value;
    value = 0; map(obj->get("int8p"), value);  BOOST_CHECK_EQUAL(value,  3);
    value = 0; map(obj->get("int8n"), value);  BOOST_CHECK_EQUAL(value, -3);
    value = 0; map(obj->get("int16p"), value); BOOST_CHECK_EQUAL(value,  200);
    value = 0; map(obj->get("int16n"), value); BOOST_CHECK_EQUAL(value, -200);
    BOOST_CHECK_EXCEPTION(map(obj->get("int32p"), value), Poco::RangeException, tooLargeCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("int32n"), value), Poco::RangeException, tooSmallCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("int64p"), value), Poco::RangeException, tooLargeCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("int64n"), value), Poco::RangeException, tooSmallCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("uint64"), value), Poco::RangeException, tooLargeCheck);
}

BOOST_AUTO_TEST_CASE(Int32RangeCheck)
{
    static auto json = R"({
        "int8p"  :  3,
        "int8n"  : -3,
        "int16p" :  200,
        "int16n" : -200,
        "int32p" :  33000,
        "int32n" : -33000,
        "int64p" :  3000000000,
        "int64n" : -3000000000,
        "uint64" : 18446744073709551615
    })";
    auto tree = parse(json);
    auto obj = tree.extract<Poco::JSON::Object::Ptr>();
    int32_t value;
    value = 0; map(obj->get("int8p"), value);  BOOST_CHECK_EQUAL(value,  3);
    value = 0; map(obj->get("int8n"), value);  BOOST_CHECK_EQUAL(value, -3);
    value = 0; map(obj->get("int16p"), value); BOOST_CHECK_EQUAL(value,  200);
    value = 0; map(obj->get("int16n"), value); BOOST_CHECK_EQUAL(value, -200);
    value = 0; map(obj->get("int32p"), value); BOOST_CHECK_EQUAL(value,  33000);
    value = 0; map(obj->get("int32n"), value); BOOST_CHECK_EQUAL(value, -33000);
    BOOST_CHECK_EXCEPTION(map(obj->get("int64p"), value), Poco::RangeException, tooLargeCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("int64n"), value), Poco::RangeException, tooSmallCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("uint64"), value), Poco::RangeException, tooLargeCheck);
}

BOOST_AUTO_TEST_CASE(Int64RangeCheck)
{
    static auto json = R"({
        "int8p"  :  3,
        "int8n"  : -3,
        "int16p" :  200,
        "int16n" : -200,
        "int32p" :  33000,
        "int32n" : -33000,
        "int64p" :  3000000000,
        "int64n" : -3000000000,
        "uint64" : 18446744073709551615
    })";
    auto tree = parse(json);
    auto obj = tree.extract<Poco::JSON::Object::Ptr>();
    int64_t value;
    value = 0; map(obj->get("int8p"), value);  BOOST_CHECK_EQUAL(value,  3);
    value = 0; map(obj->get("int8n"), value);  BOOST_CHECK_EQUAL(value, -3);
    value = 0; map(obj->get("int16p"), value); BOOST_CHECK_EQUAL(value,  200);
    value = 0; map(obj->get("int16n"), value); BOOST_CHECK_EQUAL(value, -200);
    value = 0; map(obj->get("int32p"), value); BOOST_CHECK_EQUAL(value,  33000);
    value = 0; map(obj->get("int32n"), value); BOOST_CHECK_EQUAL(value, -33000);
    value = 0; map(obj->get("int64p"), value); BOOST_CHECK_EQUAL(value,  3000000000);
    value = 0; map(obj->get("int64n"), value); BOOST_CHECK_EQUAL(value, -3000000000);
    BOOST_CHECK_EXCEPTION(map(obj->get("uint64"), value), Poco::RangeException, tooLargeCheck);
}

BOOST_AUTO_TEST_CASE(UInt16RangeCheck)
{
    static auto json = R"({
        "uint8p"  :  3,
        "uint8n"  : -3,
        "uint16p" :  300,
        "uint16n" : -300,
        "uint32p" :  66000,
        "uint32n" : -66000,
        "uint64p" :  5000000000,
        "uint64n" : -5000000000,
        "uint64"  : 18446744073709551615
    })";
    auto tree = parse(json);
    auto obj = tree.extract<Poco::JSON::Object::Ptr>();
    uint16_t value;
    value = 0; map(obj->get("uint8p"), value);  BOOST_CHECK_EQUAL(value,  3);
    BOOST_CHECK_EXCEPTION(map(obj->get("uint8n"), value), bad_type_exception, uintCheck);
    value = 0; map(obj->get("uint16p"), value); BOOST_CHECK_EQUAL(value,  300);
    BOOST_CHECK_EXCEPTION(map(obj->get("uint16n"), value), bad_type_exception, uintCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("uint32p"), value), Poco::RangeException, tooLargeCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("uint32n"), value), bad_type_exception, uintCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("uint64p"), value), Poco::RangeException, tooLargeCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("uint64n"), value), bad_type_exception, uintCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("uint64"), value), Poco::RangeException, tooLargeCheck);
}

BOOST_AUTO_TEST_CASE(UInt32RangeCheck)
{
    static auto json = R"({
        "uint8p"  :  3,
        "uint8n"  : -3,
        "uint16p" :  300,
        "uint16n" : -300,
        "uint32p" :  66000,
        "uint32n" : -66000,
        "uint64p" :  5000000000,
        "uint64n" : -5000000000,
        "uint64"  : 18446744073709551615
    })";
    auto tree = parse(json);
    auto obj = tree.extract<Poco::JSON::Object::Ptr>();
    uint32_t value;
    value = 0; map(obj->get("uint8p"), value);  BOOST_CHECK_EQUAL(value,  3);
    BOOST_CHECK_EXCEPTION(map(obj->get("uint8n"), value), bad_type_exception, uintCheck);
    value = 0; map(obj->get("uint16p"), value); BOOST_CHECK_EQUAL(value,  300);
    BOOST_CHECK_EXCEPTION(map(obj->get("uint16n"), value), bad_type_exception, uintCheck);
    value = 0; map(obj->get("uint32p"), value); BOOST_CHECK_EQUAL(value,  66000);
    BOOST_CHECK_EXCEPTION(map(obj->get("uint32n"), value), bad_type_exception, uintCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("uint64p"), value), Poco::RangeException, tooLargeCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("uint64n"), value), bad_type_exception, uintCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("uint64"), value), Poco::RangeException, tooLargeCheck);
}

BOOST_AUTO_TEST_CASE(UInt64RangeCheck)
{
    static auto json = R"({
        "uint8p"  :  3,
        "uint8n"  : -3,
        "uint16p" :  300,
        "uint16n" : -300,
        "uint32p" :  66000,
        "uint32n" : -66000,
        "uint64p" :  5000000000,
        "uint64n" : -5000000000,
        "uint64"  : 18446744073709551615
    })";
    auto tree = parse(json);
    auto obj = tree.extract<Poco::JSON::Object::Ptr>();
    uint64_t value;
    value = 0; map(obj->get("uint8p"), value);  BOOST_CHECK_EQUAL(value,  3);
    BOOST_CHECK_EXCEPTION(map(obj->get("uint8n"), value), bad_type_exception, uintCheck);
    value = 0; map(obj->get("uint16p"), value); BOOST_CHECK_EQUAL(value,  300);
    BOOST_CHECK_EXCEPTION(map(obj->get("uint16n"), value), bad_type_exception, uintCheck);
    value = 0; map(obj->get("uint32p"), value); BOOST_CHECK_EQUAL(value,  66000);
    BOOST_CHECK_EXCEPTION(map(obj->get("uint32n"), value), bad_type_exception, uintCheck);
    value = 0; map(obj->get("uint64p"), value); BOOST_CHECK_EQUAL(value,  5000000000);
    BOOST_CHECK_EXCEPTION(map(obj->get("uint64n"), value), bad_type_exception, uintCheck);
    value = 0; map(obj->get("uint64"), value); BOOST_CHECK_EQUAL(value,  18446744073709551615ULL);
}

BOOST_AUTO_TEST_CASE(BoolTypeCast)
{
    static auto json = R"({
        "number"    : 123,
        "float"     : 45.67,
        "string"    : "blah",
        "boolean"   : true,
        "array"     : [ 2, 12, 22 ],
        "object"    : { "a" : 1, "b" : 2 },
        "null"      : null
    })";
    auto tree = parse(json);
    auto obj = tree.extract<Poco::JSON::Object::Ptr>();

    bool value = false;
    BOOST_CHECK_EXCEPTION(map(obj->get("number"), value), bad_type_exception, boolCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("float"), value), bad_type_exception, boolCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("string"), value), bad_type_exception, boolCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("array"), value), bad_type_exception, boolCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("object"), value), bad_type_exception, boolCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("null"), value), bad_type_exception, boolCheck);

    map(obj->get("boolean"), value);
    BOOST_CHECK(value);
}

BOOST_AUTO_TEST_CASE(StringTypeCast)
{
    static auto json = R"({
        "number"    : 123,
        "float"     : 45.67,
        "string"    : "blah",
        "boolean"   : true,
        "array"     : [ 2, 12, 22 ],
        "object"    : { "a" : 1, "b" : 2 },
        "null"      : null
    })";
    auto tree = parse(json);
    auto obj = tree.extract<Poco::JSON::Object::Ptr>();

    std::string value;
    BOOST_CHECK_EXCEPTION(map(obj->get("number"), value), bad_type_exception, stringCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("float"), value), bad_type_exception, stringCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("boolean"), value), bad_type_exception, stringCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("array"), value), bad_type_exception, stringCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("object"), value), bad_type_exception, stringCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("null"), value), bad_type_exception, stringCheck);

    map(obj->get("string"), value);
    BOOST_CHECK_EQUAL(value, "blah");
}

BOOST_AUTO_TEST_CASE(SimpleStructOk)
{
    static auto json = R"({
        "int": 22,
        "str": "hello"
    })";
    SimpleStruct s;
    s.intField = 33;
    s.strField = "bye";
    map(parse(json), s);
    BOOST_CHECK_EQUAL(s.intField, 22);
    BOOST_CHECK_EQUAL(s.strField, "hello");
}

BOOST_AUTO_TEST_CASE(SimpleStructTypeCast)
{
    static auto json = R"({
        "number"    : 123,
        "float"     : 45.67,
        "string"    : "blah",
        "boolean"   : true,
        "array"     : [ 2, 12, 22 ],
        "object"    : { "int" : 1, "str" : "foo" },
        "null"      : null
    })";
    auto tree = parse(json);
    auto obj = tree.extract<Poco::JSON::Object::Ptr>();

    SimpleStruct value;
    value.intField = 0;
    value.strField = "";

    BOOST_CHECK_EXCEPTION(map(obj->get("number"), value), bad_type_exception, structCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("float"), value), bad_type_exception, structCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("string"), value), bad_type_exception, structCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("boolean"), value), bad_type_exception, structCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("array"), value), bad_type_exception, structCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("null"), value), bad_type_exception, structCheck);

    map(obj->get("object"), value);
    BOOST_CHECK_EQUAL(value.intField, 1);
    BOOST_CHECK_EQUAL(value.strField, "foo");
}

BOOST_AUTO_TEST_CASE(SimpleStructBadType)
{
    static auto json = R"([{
        "int": 22,
        "str": "hello"
    }])";
    SimpleStruct s;
    BOOST_CHECK_EXCEPTION(map(parse(json), s), bad_type_exception, structCheck);
}

BOOST_AUTO_TEST_CASE(SimpleStructMissingRequiredField)
{
    static auto json = R"({
        "str": "hello"
    })";
    SimpleStruct s;
    BOOST_CHECK_EXCEPTION(map(parse(json), s), field_not_found, [](const field_not_found & e) {
        return e.what() == std::string("field not found: int");
    });
}

BOOST_AUTO_TEST_CASE(SimpleStructMissingOptionalField)
{
    static auto json = R"({
        "int": 22
    })";
    SimpleStruct s;
    s.strField = "default";
    map(parse(json), s);
    BOOST_CHECK_EQUAL(s.strField, "default");
}

BOOST_AUTO_TEST_CASE(SimpleStructBadIntFieldType)
{
    static auto json = R"({
        "int": "hello",
        "str": "hello"
    })";
    SimpleStruct s;
    BOOST_CHECK_EXCEPTION(map(parse(json), s), parse_error, [](const parse_error & e) {
        return e.what() == std::string("field int: is not a signed integer");
    });
}

BOOST_AUTO_TEST_CASE(SimpleStructBadStringFieldType)
{
    static auto json = R"({
        "int": 22,
        "str": 33
    })";
    SimpleStruct s;
    BOOST_CHECK_EXCEPTION(map(parse(json), s), parse_error, [](const parse_error &e){
        return e.what() == std::string("field str: is not a string");
    });
}

BOOST_AUTO_TEST_CASE(ListInt)
{
    static auto json = R"([
        2, 12, 22, 22
    ])";
    static const std::list<int> expected { 2, 12, 22, 22 };
    std::list<int> value;
    map(parse(json), value);
    BOOST_CHECK_EQUAL_COLLECTIONS(value.begin(), value.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(VectorInt)
{
    static auto json = R"([
        2, 12, 22, 22
    ])";
    static const std::vector<int> expected { 2, 12, 22, 22 };
    std::vector<int> value;
    map(parse(json), value);
    BOOST_CHECK_EQUAL_COLLECTIONS(value.begin(), value.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(VectorIntEmpty)
{
    static auto json = R"([
    ])";
    static const std::vector<int> expected { };
    std::vector<int> value;
    map(parse(json), value);
    BOOST_CHECK_EQUAL_COLLECTIONS(value.begin(), value.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(SetInt)
{
    static auto json = R"([
        2, 12, 22, 22
    ])";
    static const std::set<int> expected { 2, 12, 22 };
    std::set<int> value;
    map(parse(json), value);
    BOOST_CHECK_EQUAL_COLLECTIONS(value.begin(), value.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(UnorderedSetInt)
{
    static auto json = R"([
        2, 12, 22, 22
    ])";
    static const std::unordered_set<int> expected { 12, 2, 22 };
    std::unordered_set<int> value;
    map(parse(json), value);
    BOOST_CHECK_EQUAL_COLLECTIONS(value.begin(), value.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(VectorIntBadCast)
{
    static auto json = R"([
        3, 14.15
    ])";
    std::vector<int> value;
    BOOST_CHECK_EXCEPTION(map(parse(json), value), parse_error, [](const parse_error &e){
        return e.what() == std::string("item 1: is not a signed integer");
    });
}

BOOST_AUTO_TEST_CASE(VectorStruct)
{
    static auto json = R"([
        {
            "int": 1,
            "str": "foo"
        },
        {
            "int": 2,
            "str": "bar"
        }
    ])";
    static const std::vector<SimpleStruct> expected{
        { 1, "foo" },
        { 2, "bar" }
    };
    std::vector<SimpleStruct> value;
    map(parse(json), value);
    BOOST_CHECK_EQUAL_COLLECTIONS(value.begin(), value.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(VectorStructBadCast)
{
    static auto json = R"([
        {
            "int": 1,
            "str": "foo"
        },
        {
            "int": 2,
            "str": 33
        }
    ])";
    std::vector<SimpleStruct> value;
    BOOST_CHECK_EXCEPTION(map(parse(json), value), parse_error, [](const parse_error &e){
        return e.what() == std::string("item 1: field str: is not a string");
    });
}

BOOST_AUTO_TEST_CASE(StructVectorInt)
{
    static auto json = R"({
        "numbers": [
            2, 12, 22, 22
        ]
    })";
    static const std::vector<int> expected { 2, 12, 22, 22 };
    NumberStruct value;
    map(parse(json), value);
    BOOST_CHECK_EQUAL_COLLECTIONS(value.numbers.begin(), value.numbers.end(),
            expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(VectorVectorInt)
{
    static auto json = R"([
        [ 3, 14, 15, 92, 7 ],
        [ 2, 7, 1828, 1828 ]
    ])";
    static const std::vector<std::vector<int>> expected{
        { 3, 14, 15, 92, 7 },
        { 2, 7, 1828, 1828 }
    };
    std::vector<std::vector<int>> value;
    map(parse(json), value);
    BOOST_REQUIRE_EQUAL(value.size(), expected.size());
    BOOST_CHECK_EQUAL_COLLECTIONS(value[0].begin(), value[0].end(),
            expected[0].begin(), expected[0].end());
    BOOST_CHECK_EQUAL_COLLECTIONS(value[1].begin(), value[1].end(),
            expected[1].begin(), expected[1].end());
}

BOOST_AUTO_TEST_CASE(VectorListInt)
{
    static auto json = R"([
        [ 3, 14, 15, 92, 7 ],
        [ 2, 7, 1828, 1828 ]
    ])";
    static const std::vector<std::list<int>> expected{
        { 3, 14, 15, 92, 7 },
        { 2, 7, 1828, 1828 }
    };
    std::vector<std::list<int>> value;
    map(parse(json), value);
    BOOST_REQUIRE_EQUAL(value.size(), expected.size());
    BOOST_CHECK_EQUAL_COLLECTIONS(value[0].begin(), value[0].end(),
            expected[0].begin(), expected[0].end());
    BOOST_CHECK_EQUAL_COLLECTIONS(value[1].begin(), value[1].end(),
            expected[1].begin(), expected[1].end());
}

BOOST_AUTO_TEST_CASE(VectorIntTypeCast)
{
    static auto json = R"({
        "number"    : 123,
        "float"     : 45.67,
        "string"    : "blah",
        "boolean"   : true,
        "array"     : [ 2, 12, 22 ],
        "object"    : { "a" : 1, "b" : 2 },
        "null"      : null
    })";
    static const std::vector<int> expected { 2, 12, 22 };
    auto tree = parse(json);
    auto obj = tree.extract<Poco::JSON::Object::Ptr>();

    std::vector<int> value;
    BOOST_CHECK_EXCEPTION(map(obj->get("number"), value), bad_type_exception, arrCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("float"), value), bad_type_exception, arrCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("string"), value), bad_type_exception, arrCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("boolean"), value), bad_type_exception, arrCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("object"), value), bad_type_exception, arrCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("null"), value), bad_type_exception, arrCheck);

    map(obj->get("array"), value);
    BOOST_CHECK_EQUAL_COLLECTIONS(value.begin(), value.end(), expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE(JsonObjectToMapStringInt)
{
    static auto json = R"({
        "a" : 1,
        "b" : 2
    })";
    auto tree = parse(json);

    std::map<std::string, int> value;
    map(tree, value);
    BOOST_REQUIRE_EQUAL(value.size(), 2);
    BOOST_CHECK_EQUAL(value.at("a"), 1);
    BOOST_CHECK_EQUAL(value.at("b"), 2);
}

BOOST_AUTO_TEST_CASE(DynamicStructToMapStringInt)
{
    Poco::DynamicStruct source;
    source["a"] = 1;
    source["b"] = 2;

    std::map<std::string, int> value;
    map(source, value);
    BOOST_REQUIRE_EQUAL(value.size(), 2);
    BOOST_CHECK_EQUAL(value.at("a"), 1);
    BOOST_CHECK_EQUAL(value.at("b"), 2);
}

BOOST_AUTO_TEST_CASE(DynamicStructToMapStringUInt)
{
    Poco::DynamicStruct source;
    source["a"] = 1;
    source["b"] = 2;

    std::map<std::string, unsigned int> value;
    map(source, value);
    BOOST_REQUIRE_EQUAL(value.size(), 2);
    BOOST_CHECK_EQUAL(value.at("a"), 1);
    BOOST_CHECK_EQUAL(value.at("b"), 2);
}

BOOST_AUTO_TEST_CASE(JsonObjectToUnorderedMapStringInt)
{
    static auto json = R"({
        "a" : 1,
        "b" : 2
    })";
    auto tree = parse(json);

    std::unordered_map<std::string, int> value;
    map(tree, value);
    BOOST_REQUIRE_EQUAL(value.size(), 2);
    BOOST_CHECK_EQUAL(value.at("a"), 1);
    BOOST_CHECK_EQUAL(value.at("b"), 2);
}

BOOST_AUTO_TEST_CASE(JsonArrayOfObjectsToMap)
{
    static auto json = R"([
        { "key" : 1, "value" : "one" },
        { "key" : 2, "value" : "two" },
        { "key" : 3, "value" : "three" }
    ])";
    auto tree = parse(json);

    std::map<int, std::string> value;
    PairArrayToMapMapper<Key, Value, decltype(value)>::map(tree, value);
    BOOST_REQUIRE_EQUAL(value.size(), 3);
    BOOST_CHECK_EQUAL(value.at(1), "one");
    BOOST_CHECK_EQUAL(value.at(2), "two");
    BOOST_CHECK_EQUAL(value.at(3), "three");
}

BOOST_AUTO_TEST_CASE(JsonArrayOfObjectsToSubMap)
{
    static auto json = R"({
        "submap" : [
            { "key" : 1, "value" : "one" },
            { "key" : 2, "value" : "two" },
            { "key" : 3, "value" : "three" }
        ]
    })";
    auto tree = parse(json);

    StructWithMap2 value;
    map(tree, value);

    BOOST_REQUIRE_EQUAL(value.submap.size(), 3);
    BOOST_CHECK_EQUAL(value.submap.at(1), "one");
    BOOST_CHECK_EQUAL(value.submap.at(2), "two");
    BOOST_CHECK_EQUAL(value.submap.at(3), "three");
}

BOOST_AUTO_TEST_CASE(OptionalTypesNotNull)
{
    static auto json = R"({
        "std_optional"      : 1,
        "std_unique_ptr"    : 2,
        "std_shared_ptr"    : 3,
        "poco_optional"     : 4,
        "boost_optional"    : 5,
        "boost_shared_ptr"  : 6
    })";
    auto tree = parse(json);
    auto obj = tree.extract<Poco::JSON::Object::Ptr>();

    OptionalStruct value;
    map(obj, value);

#if __cplusplus >= 201703L
    BOOST_REQUIRE(value.std_optional.has_value());
    BOOST_CHECK_EQUAL(value.std_optional.value(), 1);
#endif
    BOOST_REQUIRE(value.std_unique_ptr);
    BOOST_CHECK_EQUAL(*value.std_unique_ptr, 2);
    BOOST_REQUIRE(value.std_shared_ptr);
    BOOST_CHECK_EQUAL(*value.std_shared_ptr, 3);
    BOOST_REQUIRE(value.poco_optional.isSpecified());
    BOOST_CHECK_EQUAL(value.poco_optional.value(), 4);
    BOOST_REQUIRE(value.boost_optional.is_initialized());
    BOOST_CHECK_EQUAL(value.boost_optional.value(), 5);
    BOOST_REQUIRE(value.boost_shared_ptr);
    BOOST_CHECK_EQUAL(*value.boost_shared_ptr, 6);
}

BOOST_AUTO_TEST_CASE(OptionalTypesNull)
{
    static auto json = R"({
        "std_optional"      : null,
        "std_unique_ptr"    : null,
        "std_shared_ptr"    : null,
        "poco_optional"     : null,
        "boost_optional"    : null,
        "boost_shared_ptr"  : null
    })";
    auto tree = parse(json);
    auto obj = tree.extract<Poco::JSON::Object::Ptr>();

    OptionalStruct value;
#if __cplusplus >= 201703L
    value.std_optional = 0;
#endif
    value.std_unique_ptr = 0;
    value.std_shared_ptr = 0;
    value.poco_optional = 0;
    value.boost_optional = 0;
    value.boost_shared_ptr = 0;

    map(obj, value);

#if __cplusplus >= 201703L
    BOOST_CHECK(!value.std_optional.has_value());
#endif
    BOOST_CHECK(!value.std_unique_ptr);
    BOOST_CHECK(!value.std_shared_ptr);
    BOOST_CHECK(!value.poco_optional.isSpecified());
    BOOST_CHECK(!value.boost_optional.is_initialized());
    BOOST_CHECK(!value.boost_shared_ptr);
}

BOOST_AUTO_TEST_CASE(CustomType)
{
    static auto json = R"({
        "number"    : 123,
        "float"     : 45.67,
        "string"    : "",
        "boolean"   : true,
        "array"     : [ 2, 12, 22 ],
        "object"    : { "a" : 1, "b" : 2 },
        "null"      : null,
        "datetime"  : "2019-11-16 20:22:15 +0300"
    })";
    auto tree = parse(json);
    auto obj = tree.extract<Poco::JSON::Object::Ptr>();

    Poco::DateTime value;

    BOOST_CHECK_EXCEPTION(map(obj->get("number"), value), bad_type_exception, stringCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("float"), value), bad_type_exception, stringCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("boolean"), value), bad_type_exception, stringCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("array"), value), bad_type_exception, stringCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("object"), value), bad_type_exception, stringCheck);
    BOOST_CHECK_EXCEPTION(map(obj->get("null"), value), bad_type_exception, stringCheck);

    BOOST_CHECK_EXCEPTION(map(obj->get("string"), value), Poco::SyntaxException, trueCheck);

    map(obj->get("datetime"), value);
    BOOST_CHECK_EQUAL(value.year(), 2019);
    BOOST_CHECK_EQUAL(value.month(), 11);
    BOOST_CHECK_EQUAL(value.day(), 16);
    BOOST_CHECK_EQUAL(value.hour(), 17);
    BOOST_CHECK_EQUAL(value.minute(), 22);
    BOOST_CHECK_EQUAL(value.second(), 15);
}

BOOST_AUTO_TEST_CASE(PocoDynamicVar)
{
    static auto json = R"({
        "number"    : 123,
        "float"     : 45.67,
        "string"    : "blah",
        "boolean"   : true,
        "array"     : [ 2, 12, 22 ],
        "object"    : { "a" : 1, "b" : 2 },
        "null"      : null
    })";
    std::vector<int> arrExpected{ 2, 12, 22 };
    auto tree = parse(json);

    DynamicStruct value;
    map(tree, value);

    BOOST_REQUIRE(!value.v_number.isEmpty());
    BOOST_CHECK_EQUAL(value.v_number.extract<int64_t>(), 123);
    BOOST_REQUIRE(!value.v_float.isEmpty());
    BOOST_CHECK_EQUAL(value.v_float.extract<double>(), 45.67);
    BOOST_REQUIRE(!value.v_string.isEmpty());
    BOOST_CHECK_EQUAL(value.v_string.extract<std::string>(), "blah");
    BOOST_REQUIRE(!value.v_boolean.isEmpty());
    BOOST_CHECK_EQUAL(value.v_boolean.extract<bool>(), true);

    BOOST_REQUIRE(!value.v_array.isEmpty());
    BOOST_REQUIRE(value.v_array.type() == typeid(Poco::JSON::Array::Ptr));
    const auto arrp = value.v_array.extract<Poco::JSON::Array::Ptr>();
    const auto & arr = *arrp;
    BOOST_REQUIRE_EQUAL(arr.size(), arrExpected.size());
    BOOST_CHECK_EQUAL(arr.get(0).extract<int64_t>(), arrExpected[0]);
    BOOST_CHECK_EQUAL(arr.get(1).extract<int64_t>(), arrExpected[1]);
    BOOST_CHECK_EQUAL(arr.get(2).extract<int64_t>(), arrExpected[2]);

    BOOST_REQUIRE(!value.v_object.isEmpty());
    BOOST_REQUIRE(value.v_object.type() == typeid(Poco::JSON::Object::Ptr));
    const auto obj = value.v_object.extract<Poco::JSON::Object::Ptr>();
    BOOST_REQUIRE(obj->has("a"));
    BOOST_REQUIRE(obj->has("b"));

    BOOST_CHECK(value.v_null.isEmpty());
    BOOST_CHECK(value.v_opt_null.isEmpty());
}

BOOST_AUTO_TEST_CASE(PocoDynamicStructSource)
{
    static auto json = R"({
        "num" : [ 3, 14 ],
        "sub" : {
            "int" : 22,
            "str" : "hello"
        }
    })";
    std::list<int> exp{ 3, 14 };
    auto tree = parse(json);
    Poco::DynamicStruct source = Poco::JSON::Object::makeStruct(tree.extract<Poco::JSON::Object::Ptr>());

    ListAndSimple value;
    map(source, value);
    BOOST_CHECK_EQUAL_COLLECTIONS(value.num.begin(), value.num.end(), exp.begin(), exp.end());
    BOOST_CHECK_EQUAL(value.sub.intField, 22);
    BOOST_CHECK_EQUAL(value.sub.strField, "hello");
}

BOOST_AUTO_TEST_CASE(PocoDynamicStructDestination)
{
    static auto json = R"({
        "num" : [ 3, 14 ],
        "sub" : {
            "int" : 22,
            "str" : "hello"
        }
    })";
    auto tree = parse(json);

    Poco::DynamicStruct value;
    map(tree, value);

    BOOST_REQUIRE(value.contains("num"));
    BOOST_REQUIRE(value.contains("sub"));
    Poco::Dynamic::Var num, sub;
    num = value["num"];
    sub = value["sub"];
    BOOST_REQUIRE(num.isArray());
    BOOST_REQUIRE(num.size() == 2);
    BOOST_CHECK_EQUAL(num[0].convert<int>(), 3);
    BOOST_CHECK_EQUAL(num[1].convert<int>(), 14);
    BOOST_REQUIRE(sub.isStruct());
    BOOST_CHECK_EQUAL(sub["int"].convert<int>(), 22);
    BOOST_CHECK_EQUAL(sub["str"].convert<std::string>(), "hello");
}

BOOST_AUTO_TEST_CASE(PocoDynamicStructToMap)
{
    static auto json = R"({
        "a" : 1,
        "b" : 2
    })";
    auto tree = parse(json);
    Poco::DynamicStruct source = Poco::JSON::Object::makeStruct(tree.extract<Poco::JSON::Object::Ptr>());

    std::map<std::string, int> value;
    map(source, value);
    BOOST_REQUIRE_EQUAL(value.size(), 2);
    BOOST_CHECK_EQUAL(value.at("a"), 1);
    BOOST_CHECK_EQUAL(value.at("b"), 2);
}

BOOST_AUTO_TEST_CASE(PocoDynamicStructToSubMap)
{
    static auto json = R"({
        "submap" : {
            "a" : 1,
            "b" : 2
        }
    })";
    auto tree = parse(json);
    Poco::DynamicStruct source = Poco::JSON::Object::makeStruct(tree.extract<Poco::JSON::Object::Ptr>());

    StructWithMap value;
    map(source, value);
    BOOST_REQUIRE_EQUAL(value.submap.size(), 2);
    BOOST_CHECK_EQUAL(value.submap.at("a"), 1);
    BOOST_CHECK_EQUAL(value.submap.at("b"), 2);
}

BOOST_AUTO_TEST_CASE(MapOptionalFieldReturn)
{
    static auto json1 = R"({
        "real_name" : "John Smith"
    })";
    static auto json2 = R"({
    })";
    auto tree1 = parse(json1);
    auto tree2 = parse(json2);

    AlternatingStruct value;
    map(tree1, value);
    BOOST_CHECK_EQUAL(value.realName, "John Smith");
    BOOST_CHECK_EQUAL(value.pseudoName, "unknown");

    BOOST_CHECK_EXCEPTION(map(tree2, value), parse_error, [](const parse_error &e) {
        return std::string(e.what()) == "real or pseudo name must be defined";
    });
}

BOOST_AUTO_TEST_CASE(ObjectToPocoJsonObject)
{
    static auto json = R"({
        "obj" : {
            "int" : 22,
            "str" : "hello"
        }
    })";
    auto tree = parse(json);

    StructWithJsonObject value;
    map(tree, value);

    BOOST_REQUIRE(value.obj.has("int"));
    BOOST_REQUIRE_EQUAL(value.obj.get("int").extract<int64_t>(), 22);
    BOOST_REQUIRE(value.obj.has("str"));
    BOOST_REQUIRE_EQUAL(value.obj.get("str").extract<std::string>(), "hello");
}

BOOST_AUTO_TEST_SUITE_END()
