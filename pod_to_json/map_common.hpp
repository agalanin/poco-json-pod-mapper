//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Alexander Galanin <al@galanin.nnov.ru>
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include "mapper.hpp"

namespace pod_to_json
{

template < const char * KeyLabel, const char * ValueLabel, class T >
void mapMapToArray(const T & from, Poco::Dynamic::Var & to)
{
    Poco::Dynamic::Array arr;
    arr.reserve(from.size());
    for (const auto & item : from)
    {
        Poco::Dynamic::Var key, value;
        Mapper<typename T::key_type>::map(item.first, key);
        Mapper<typename T::mapped_type>::map(item.second, value);
        Poco::JSON::Object::Ptr obj = new Poco::JSON::Object(Poco::JSON_PRESERVE_KEY_ORDER);
        obj->set(KeyLabel, key);
        obj->set(ValueLabel, value);
        arr.push_back(obj);
    }
    to = arr;
}

template < const char * KeyLabel, const char * ValueLabel, typename T >
struct MapToPairArrayMapper
{
    static void map(const T & from, Poco::Dynamic::Var & to)
    {
        mapMapToArray<KeyLabel, ValueLabel>(from, to);
    }
};

}
