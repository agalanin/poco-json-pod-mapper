//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019-2020 Alexander Galanin <al@galanin.nnov.ru>
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
//////////////////////////////////////////////////////////////////////////////

#pragma once

#include <functional>
#include <string>

#include <Poco/Dynamic/Var.h>
#include <Poco/JSON/Object.h>

#include "../json_to_pod/version.hpp"

namespace pod_to_json
{

class TargetAdapter
{
    const std::function<void (const std::string &, const Poco::Dynamic::Var &)> _set;
public:
    TargetAdapter(Poco::JSON::Object & obj) :
        _set([&obj](const std::string & key, const Poco::Dynamic::Var & val) { obj.set(key, val); })
    {}

    TargetAdapter(Poco::DynamicStruct & ds) :
        _set([&ds](const std::string & key, const Poco::Dynamic::Var & val) { ds[key] = val; })
    {}

    void set(const std::string & key, const Poco::Dynamic::Var & val) const { _set(key, val); }
};

template < typename T >
struct StructMapper
{
    static void mapStruct(const T & from, TargetAdapter & to) = delete;
};

template < typename T >
struct Mapper
{
    static void map(const T & from, Poco::Dynamic::Var & to)
    {
        Poco::JSON::Object::Ptr obj = new Poco::JSON::Object(Poco::JSON_PRESERVE_KEY_ORDER);
        TargetAdapter adapter(*obj);
        StructMapper<T>::mapStruct(from, adapter);
        to = obj;
    }
};

template < typename T >
static void map(const T & from, Poco::Dynamic::Var & to)
{
    Mapper<T>::map(from, to);
}

template < typename T >
static void map(const T & from, Poco::DynamicStruct & to)
{
    TargetAdapter adapter(to);
    StructMapper<T>::mapStruct(from, adapter);
}

template < typename T >
static void map(const T & from, Poco::JSON::Object & to)
{
    TargetAdapter adapter(to);
    StructMapper<T>::mapStruct(from, adapter);
}

template <>
struct Mapper<int16_t>
{
    static void map(const int16_t & from, Poco::Dynamic::Var & to)
    {
        to = from;
    }
};

template <>
struct Mapper<int32_t>
{
    static void map(const int32_t & from, Poco::Dynamic::Var & to)
    {
        to = from;
    }
};

template <>
struct Mapper<int64_t>
{
    static void map(const int64_t & from, Poco::Dynamic::Var & to)
    {
        to = from;
    }
};

template <>
struct Mapper<uint16_t>
{
    static void map(const uint16_t & from, Poco::Dynamic::Var & to)
    {
        to = from;
    }
};

template <>
struct Mapper<uint32_t>
{
    static void map(const uint32_t & from, Poco::Dynamic::Var & to)
    {
        to = from;
    }
};

template <>
struct Mapper<uint64_t>
{
    static void map(const uint64_t & from, Poco::Dynamic::Var & to)
    {
        to = from;
    }
};

template <>
struct Mapper<float>
{
    static void map(const float & from, Poco::Dynamic::Var & to)
    {
        to = from;
    }
};

template <>
struct Mapper<double>
{
    static void map(const double & from, Poco::Dynamic::Var & to)
    {
        to = from;
    }
};

template <>
struct Mapper<bool>
{
    static void map(const bool & from, Poco::Dynamic::Var & to)
    {
        to = from;
    }
};

template <>
struct Mapper<std::string>
{
    static void map(const std::string & from, Poco::Dynamic::Var & to)
    {
        to = from;
    }
};

template < typename T, typename MapperClass = Mapper<T> >
void mapField(TargetAdapter & to, const std::string & key, const T & from)
{
    Poco::Dynamic::Var var;
    MapperClass::map(from, var);
    to.set(key, var);
}

}
