DEST=poco-json-pod-mapper
prefix=/usr/local
includedir=$(prefix)/include
datarootdir=$(prefix)/share
docdir=$(datarootdir)/doc/$(DEST)

VER_MAJOR=$(shell grep POCO_JSON_POD_MAPPER_VERSION_MAJOR json_to_pod/version.hpp | awk '{print $$3}')
VER_MINOR=$(shell grep POCO_JSON_POD_MAPPER_VERSION_MINOR json_to_pod/version.hpp | awk '{print $$3}')
VER_PATCH=$(shell grep POCO_JSON_POD_MAPPER_VERSION_PATCH json_to_pod/version.hpp | awk '{print $$3}')

all:

install:
	mkdir -p "$(DESTDIR)$(includedir)"
	cp -r json_to_pod pod_to_json "$(DESTDIR)$(includedir)"
	find "$(DESTDIR)$(includedir)/json_to_pod" -type d -exec chmod 755 {} \;
	find "$(DESTDIR)$(includedir)/json_to_pod" -type f -exec chmod 644 {} \;
	find "$(DESTDIR)$(includedir)/pod_to_json" -type d -exec chmod 755 {} \;
	find "$(DESTDIR)$(includedir)/pod_to_json" -type f -exec chmod 644 {} \;
	mkdir -p "$(DESTDIR)$(docdir)"
	install -m 644 -t "$(DESTDIR)$(docdir)" README.md LICENSE.md

check:
	$(MAKE) -C test clean test CXX_STD=c++11
	$(MAKE) -C test clean test CXX_STD=c++17

archive:
	hg archive -t tgz $(DEST)-$(VER_MAJOR).$(VER_MINOR).$(VER_PATCH).tar.gz

.PHONY: all install check archive
